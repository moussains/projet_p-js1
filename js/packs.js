/*  Permet d'afficher les coffres*/
function lesCoffres(){
    $("#lescoffres").css("visibility","visible");
}
/*  Permet d'initialiser l'affichage des cartes et les raretés*/
function initialiseCarte(){
    for (var x = 0; x < 4; x++) {
        $("#fond_img"+x).attr('src','images/lescartes/fond_img.png');
        $("#fond_img"+x).attr('title','Cliquez pour obtenir cette chance');
        $("#reponse"+x).empty().append();
    }
}


/*  Permet d'ouvrir les coffres*/
function ouvrirCoffre(id, titreCof, imgCof){
    //On initialise les cartes
    initialiseCarte();
    //On affiche le nom du coffre dans le modal
    $("#nom_cof").empty().append(titreCof);
    //On affiche la photo du coffre choisi
    $("#img_cof").attr('src','images/lescoffres/'+imgCof);
    //On génére les chances pour la coffre choisi
    chancePacks(id);
    //On affiche le modal
    $("#lespacks").modal("show");
}


/*  Après avoir cliquer sur une carte on récupère sa face dans la base de données*/
function apresClickCarte(id_Col,id_carte_rare,id_NomCarte)
{    
    /*setTimeout(function(){*/
        $.get( 
            "classes/classes.php", { 
                selectionner : '',
                id_rar: id_carte_rare
            },
            function(data) {
                var dataJson = JSON.parse(data);
                $(id_Col).attr('src','images/lescartes/'+dataJson.photoCollection);
                $(id_Col).removeAttr('onclick');
                $("#reponse"+id_NomCarte).empty().append("<strong style='color:"+dataJson.couleur_rarete+";'>"+dataJson.rarete+"</strong>");
                $("#fond_img"+id_NomCarte).attr('title',dataJson.collection);
            }
        );/*
    },3000);*/
}



/*  Permets de jouer les chances de pourcentages du coffre choisi*/
function chancePacks(id_coffre){

    switch (id_coffre)
    {
        //Si coffre 1 (coffre gratuit)
        case 1 :
            //Commun    :   80%
            //Rare      :   15%
            //Épique    :   5%
            getChancepourcentage(80, 15, 5);
        break;

        //Si coffre 2 (coffre de bataille)
        case 2 :
            //Commun    :   65%
            //Rare      :   26%
            //Épique    :   9%
            getChancepourcentage(65, 26, 9);
        break;

        //Si coffre 3 (coffre maigre)
        case 3 :
            //Commun    :   53%
            //Rare      :   35%
            //Épique    :   12%
            getChancepourcentage(53, 35, 12);
        break;

        //Si coffre 4 (coffre ample)
        case 4 :
            //Commun    :   38%
            //Rare      :   44%
            //Épique    :   18%
            getChancepourcentage(38, 44, 18);
        break;

        //Si coffre 5 (coffre généreux)
        case 5 :
            //Commun    :   29%
            //Rare      :   48%
            //Épique    :   23%
            getChancepourcentage(29, 48, 23);
        break;

        //Si coffre 6 (coffre précieux)
        case 6 :
            //Commun    :   20%
            //Rare      :   47%
            //Épique    :   33%
            getChancepourcentage(20, 47, 33);
        break;

        //Si coffre 7 (coffre splendide)
        case 7 :
            //Commun    :   15%
            //Rare      :   35%
            //Épique    :   50%
            getChancepourcentage(15, 35, 50);
        break;

        //Si coffre 8 (coffre extravagant)
        case 8 :
            //Commun    :   9%
            //Rare      :   18%
            //Épique    :   73%
            getChancepourcentage(9, 18, 73);
        break;
    }
}

/*  Permet de générer le radom*/
function tableRandom(array)
{
    var i = 0, length = array.length, swap = 0, temp = '';
    for (i; i < length; i++ )
    {
        swap= Math.floor(Math.random() * (i + 1));
        temp= array[swap];
        array[swap] = array[i];
        array[i]= temp;
    }
    return array;
};

/*  Permet de récupérer les données du random*/
function chancePourcentage(values, chances)
{
    /*On crée un tableau*/
    var pool = [];

    /*On parcours les chances*/
    for ( var i = 0; i < chances.length; i++ )
    {
        for ( var j = 0; j < chances[i]; j++ )
        {
            pool.push(i);
        }
    }
    return values[tableRandom(pool)['0']];
};


/* Récupération des pourcentages pour les raretés selon la coffre choisi*/
function getChancepourcentage(val_commun, val_rare, val_epique){
    var les_elements = "";
    /*On parcourt pour 4 carte*/
    for ( var x = 0; x < 4; x++ )
    {
        var chance = chancePourcentage([1, 2, 3], [val_commun, val_rare, val_epique]);
        les_elements += chance;
        $("#fond_img"+x).attr('onclick','apresClickCarte(this,'+chance+','+x+')');

    }   

    // S'il y a pas de carte rare, on vérifie et on remplace la deuxième carte par une carte rare.
    if (les_elements.indexOf("2") == -1) {
        $("#fond_img"+1).attr('onclick','apresClickCarte(this,2,1)');
        console.log("Pas de chance pour rare, on rajoute donc au moins le rare à la deuxième carte");
    }else{
    }   
}