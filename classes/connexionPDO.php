<?php
/********** CONNEXION PDO *************/
class ConnectionPDO{
	public function getConnexionPDO(){
		//En cas succès
		try{
			//Nous allons établir une connexion de notre base de données
			$tchat_js= new PDO('mysql:host=185.98.131.90;dbname=ascmt1089731_9wlwtq;charset=utf8', 'ascmt1089731_9wlwtq', 'ascmsite');
			$tchat_js->query(" SET NAMES utf8");
			$tchat_js->query(" SET lc_time_names = 'fr_FR'");
			return $tchat_js;
		}//En cas d'erreur
		catch (Exception $e){
			die('Erreur lors de connexion de BDD: ' . $e->getMessage());
		}
	}
}