<?php 
require_once "connexionPDO.php";

/********** CLASSE Coffre *************/
Class Coffre{

	public function getCoffre(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM coffre");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}
}
$Coffre = new Coffre();
$coffres = $Coffre->getCoffre();


/********** CLASSE Collection *************/
Class Collection{

	public function getCollection(){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM collection C INNER JOIN rarete R ON C.id_rar = R.id_rar ORDER BY rang");
		$req->execute(); 
		$donnees = $req->fetchAll();
		return $donnees;
	}

	/*Récupérer une valeur au hasard*/
	public function getCollectionRandom($id){
		//Connexion PDO
		$connect = new ConnectionPDO();
		$connect = $connect->getConnexionPDO();

		$req = $connect->prepare("SELECT * FROM collection C INNER JOIN rarete R ON C.id_rar = R.id_rar WHERE R.id_rar = ".$id." ORDER BY RAND() LIMIT 1");
		$req->execute(); 
		$donnees = $req->fetch();
		return $donnees;
	}
}
$Collection = new Collection();
$collections = $Collection->getCollection();

if (isset($_GET['selectionner'])) {
	$idRar = $_GET['id_rar'];
	$col =  $Collection->getCollectionRandom($idRar);
	
	$arr = array('rarete' => $col['libelle_rar'], 'photoCollection' => $col['photo_col'],'couleur_rarete'=>$col['couleur'], 'collection'=>$col['libelle_col']);
	echo json_encode($arr);
}