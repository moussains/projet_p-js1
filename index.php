<?php
    require_once "classes/classes.php";
?>
<!DOCTYPE html>
<html  >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="assets/images/unnamed.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Bomber friends</title>
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">

        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <!-- SECTION HEADER -->
        <section class="menu cid-rOrpH9uPcg" once="menu" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm ">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="./"><img src="assets/images/unnamed.png" alt="Logo_Bomber" title="Accueil" style="height: 4.8rem;"></a>
                        </span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                    <div class="navbar-buttons mbr-section-btn">
                        <a class="btn display-5 btn_orange" href="packs.php"><span class="mbri-magic-stick mbr-iconfont mbr-iconfont-btn"></span>PACKS</a>
                    </div>
                    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="https://play.google.com/store/apps/details?id=com.hyperkani.bomberfriends&hl=fr" target="_blank">
                                <span class="mbri-save mbr-iconfont mbr-iconfont-btn"></span>
                                Télécharger
                                <br>
                                sous Android
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="header8 cid-rOrpFw2s0y border_orange" data-bg-video="https://www.youtube.com/watch?v=wHAbxYgdyxI" id="header8-0">
            <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(118, 118, 118);"></div>
            <div class="container align-center">
                <div class="row justify-content-md-center">
                    <div class="mbr-white col-md-10">
                        <p>
                            <img src="images/title.png" class="img-fluid">
                        </p>
                        <p class="mbr-text align-center py-2 mbr-fonts-style display-5">
                            Jouer immédiatement en un seul clic
                        </p>
                        <div class="mbr-section-btn text-center">
                            <a class="btn display-2 btn_vert" href="https://www.1001jeux.fr/action/bomber-friends" target="_blank">JOUER</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="features8 cid-rOw21f3Ap4 mbr-parallax-background border_orange" id="features8-7" >
            <div class="mbr-overlay" style="opacity: 0.7; background-color: rgb(35, 35, 35);"></div>
            <div class="container">
                <h1 class="mbr-section-title align-center py-2 mbr-bold mbr-fonts-style display-1 degrade" >BOMBER FRIENDS</h1>
                <p class="text-center display-7">
                    Bomber Friends est un jeu de type stratégie dérivée de la célèbre série de jeu vidéo Bomberman. Le jeu est international et est joué dans de nombreux pays.
                </p>
                <div class="media-container-row">
                    <div class="card  col-12 col-md-6">
                        <div class="card-box align-center">
                            <img class="img-fluid" src="images/but_logo.png" style="width: 46px;"><br>
                            <h4 class="card-title mbr-fonts-style display-3">BUT DU JEU</h4>
                            <p class="mbr-text mbr-fonts-style display-7">
                                Découvrir le but du jeu
                            </p>
                            <div class="mbr-section-btn text-center">
                                <a href="#" class="btn display-5 btn_orange" data-toggle="modal" data-target="#butdujeu">PLUS</a>
                            </div>
                        </div>
                    </div>
                    <div class="card  col-12 col-md-6">
                        <div class="card-box align-center">
                            <img class="img-fluid" src="images/bombe_logo.png" style="width: 46px;"><br>
                            <h4 class="card-title mbr-fonts-style display-3">COLLECTION DE BONUS</h4>
                            <p class="mbr-text mbr-fonts-style display-7">
                                Découvrir les différentes bonus
                            </p>
                            <div class="mbr-section-btn text-center">
                                <a href="#" class="btn display-5 btn_orange" data-toggle="modal" data-target="#lescollections">PLUS</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section once="footers" class="cid-rOvBr1NiQ0 mbr-reveal" id="footer7-4">
            <div class="container">
                <div class="media-container-row align-center mbr-white">
                    <div class="row row-copirayt">
                        <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                            © Copyright 2020 Bomber Friends. Site created by Moussa !
                        </p>
                    </div>
                </div>
            </div>
        </section>



        <!-- The Modal for le but -->
        <div class="modal fade" id="butdujeu">
            <div class="modal-dialog  modal-xl">
                <div class="modal-content card-box bdj">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <h4 class="modal-title">BUT DU JEU</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <h3>Mode aventure </h3>
                        <p>
                            Le but est ici d'avancer dans plusieurs niveaux en évitant les ennemis pour trouver la clé cachée (elle se révèle quand on marche sur la trappe de sortie verrouillée) et sortir du niveau. Les niveaux sont de plus en plus durs, avec des fantômes apparaissant au bout d'un certain nombre de niveaux pour vous compliquer la tâche. Des séries de niveaux spéciaux apparaissent également pour faire gagner des costumes (Quête du Robot et Quête Médiévale).
                        </p>
                        <h3>Mode multijoueur  </h3>
                        <p>
                            En mode multijoueur, une partie est créé par un joueur (à 2 joueurs et à 4 joueurs où 12 types de stages sont disponibles, à 6 joueurs ou à 8 joueurs où 3 types de stages sont disponibles ou bien en privé, où il faudra alors un mot de passe pour entrer dans la partie) et les joueurs s'affrontent dans une partie durant 1 minute 10 où il faudra tuer tous ses adversaires pour gagner la partie. Lorsqu’il ne reste plus que 30 secondes de jeu, des murs tuant instantanément les joueurs commencent à tomber sur l'arène et restreignent progressivement la largeur de jeu.
                        </p>
                        <h3>Classique </h3>
                        <p>
                            En mode classique le jeu est chacun pour soi et le but est de tuer tous tes adversaires pour gagner la partie.
                        </p>
                        <h3>Reversi  </h3>
                        <p>
                            Le but dans ce mode de jeu est de recouvrir le plus de morceau d'arène possible avec de la peinture grâce aux bombes. Les murs ne tombent pas dans ce mode. Le gagnant est celui qui recouvre le plus de peinture l'arène.
                        </p>
                        <h3>Bataille de l'équipe  </h3>
                        <p>
                            Ici les participants sont en équipe, les bleus ou les rouges, et doivent tuer tous les membres de l'autre équipe pour gagner la partie. Même si vous mourrez et que votre équipe gagne, vous gagnez. Curieusement les membres d'une même équipe peuvent se tuer entre eux, ce qui profite à certains trolls qui tue des membres de leur équipe pour les faire rager.
                        </p>

                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal for le but -->
        <div class="modal fade" id="lescollections">
            <div class="modal-dialog  modal-xl">
                <div class="modal-content card-box colle">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title text-center">COLLECTION DE BONUS</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Photo</th>
                                        <th>Description</th>
                                        <th>Rareté <span class="mbri-arrow-up mbr-iconfont mbr-iconfont-btn"></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($collections as $col): ?>
                                        <tr>
                                            <td><?php echo $col['libelle_col'].' '.$col['logo_col']; ?></td>
                                            <td><img src="images/lescartes/<?php echo $col['photo_col'];?>" class="img-fluid"></td>
                                            <td><?php echo $col['description_col']; ?></td>
                                            <td><strong style="color:<?php echo $col['couleur']; ?>;"><?php echo $col['libelle_rar']; ?></strong></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>                        
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
        </div>



        <!-- Les scritps -->
        <script src="assets/web/assets/jquery/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/dropdown/js/nav-dropdown.js"></script>
        <script src="assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <script src="assets/playervimeo/vimeo_player.js"></script>
        <script src="assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
        <script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
        <script src="assets/parallax/jarallax.min.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="assets/theme/js/script.js"></script>
    </body>
</html>
