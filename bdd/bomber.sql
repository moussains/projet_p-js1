-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  Dim 26 jan. 2020 à 00:25
-- Version du serveur :  8.0.18
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bomber`
--

-- --------------------------------------------------------

--
-- Structure de la table `coffre`
--

DROP TABLE IF EXISTS `coffre`;
CREATE TABLE IF NOT EXISTS `coffre` (
  `id_cof` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_cof` varchar(255) NOT NULL,
  PRIMARY KEY (`id_cof`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `coffre`
--

INSERT INTO `coffre` (`id_cof`, `libelle_cof`) VALUES
(1, 'Coffre gratuit'),
(2, 'Coffre de bataille'),
(3, 'Coffre maigre'),
(4, 'Coffre ample'),
(5, 'Coffre généreux'),
(6, 'Coffre précieux'),
(7, 'Coffre splendide'),
(8, 'Coffre extravagant');

-- --------------------------------------------------------

--
-- Structure de la table `collection`
--

DROP TABLE IF EXISTS `collection`;
CREATE TABLE IF NOT EXISTS `collection` (
  `id_col` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_col` varchar(255) NOT NULL,
  `description_col` text NOT NULL,
  `photo_col` varchar(255) DEFAULT NULL,
  `logo_col` varchar(255) DEFAULT NULL,
  `id_rar` int(11) NOT NULL,
  PRIMARY KEY (`id_col`),
  KEY `collection_rarete_FK` (`id_rar`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `collection`
--

INSERT INTO `collection` (`id_col`, `libelle_col`, `description_col`, `photo_col`, `logo_col`, `id_rar`) VALUES
(4, 'Bombe sans mèche', 'Ha ! La bombe n\'a même pas de mèche - elle est inoffensive ! À moins que quelqu\'un ne la fasse exploser avec une autre bombe bien sûr...', NULL, NULL, 1),
(5, 'Boite à sortilège', 'Maudissez vos adversaires avec ces abominables boites de sorcellerie concentrée ! Les effets peuvent varier.', NULL, NULL, 1),
(6, 'B.L.O.B', 'Répandez de la boue partout avec ces explosions gluantes ! Effectuez une amélioration pour de la boue plus dégueu. Beurk.', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `rarete`
--

DROP TABLE IF EXISTS `rarete`;
CREATE TABLE IF NOT EXISTS `rarete` (
  `id_rar` int(11) NOT NULL AUTO_INCREMENT,
  `libelle_rar` varchar(255) NOT NULL,
  PRIMARY KEY (`id_rar`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `rarete`
--

INSERT INTO `rarete` (`id_rar`, `libelle_rar`) VALUES
(1, 'Commun'),
(2, 'Rare'),
(3, 'Épique');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `collection`
--
ALTER TABLE `collection`
  ADD CONSTRAINT `collection_rarete_FK` FOREIGN KEY (`id_rar`) REFERENCES `rarete` (`id_rar`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
