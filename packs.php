<?php
    require_once "classes/classes.php";
?>
<!DOCTYPE html>
<html  >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
        <link rel="shortcut icon" href="assets/images/unnamed.png" type="image/x-icon">
        <meta name="description" content="">
        <title>Bomber friends</title>
        <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" href="assets/socicon/css/styles.css">
        <link rel="stylesheet" href="assets/dropdown/css/style.css">
        <link rel="stylesheet" href="assets/tether/tether.min.css">
        <link rel="stylesheet" href="assets/theme/css/style.css">
        <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
        <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/packs.css">
    </head>
    <body>
        <!-- SECTION HEADER -->
        <section class="menu cid-rOrpH9uPcg" once="menu" id="menu1-1">
            <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm ">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <div class="hamburger">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </button>
                <div class="menu-logo">
                    <div class="navbar-brand">
                        <span class="navbar-logo">
                            <a href="./"><img src="assets/images/unnamed.png" alt="Logo_Bomber" title="Accueil" style="height: 4.8rem;"></a>
                        </span>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    
                    <div class="navbar-buttons mbr-section-btn">
                        <a class="btn display-5 btn_orange" href="packs.php"><span class="mbri-magic-stick mbr-iconfont mbr-iconfont-btn"></span>PACKS</a>
                    </div>
                    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
                        <li class="nav-item">
                            <a class="nav-link link text-white display-4" href="https://play.google.com/store/apps/details?id=com.hyperkani.bomberfriends&hl=fr" target="_blank">
                                <span class="mbri-save mbr-iconfont mbr-iconfont-btn"></span>
                                Télécharger
                                <br>
                                sous Android
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </section>
        <section class="features8 section-create mbr-parallax-background border_orange" id="features8-7">
            <div class="mbr-overlay" style="opacity: 0.3; background-color: rgb(35, 35, 35);"></div>
            <div class="container">
                <h1 class="mbr-section-title align-center py-2 mbr-bold mbr-fonts-style display-1 degrade" >BOMBER FRIENDS - PACK OPENING</h1>

                <div class="media-container-row">
                    <div class="card  col-12 col-md-12">
                        <div class="card-box align-center">
                            <div class="mbr-section-btn text-center">
                                <a href="#lescoffres" class="btn display-5 btn_orange" onclick="lesCoffres();">OUVRIR DES PACKS</a>
                            </div>
                            <span id="lescoffres">
                                <p class="mbr-text mbr-fonts-style display-5" style="padding-bottom: 30px;">
                                    Choisissez une coffre
                                </p>
                                <div class="row">
                                    <?php foreach ($coffres as $cof): ?>
                                        <div class="card-body col-4 col-md-3 ml-auto">
                                            <a href="#" onclick='ouvrirCoffre(<?php echo $cof['id_cof']; ?>,"<?php echo $cof['libelle_cof']; ?>","<?php echo $cof['photo_cof']; ?>")' class="style_coffre display-7">
                                                <span class="text-center"><?php echo $cof['libelle_cof']; ?></span>
                                                <br>
                                                <img class="img-fluid coffre" src="images/lescoffres/<?php echo $cof['photo_cof']; ?>">
                                            </a>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section once="footers" class="cid-rOvBr1NiQ0 mbr-reveal" id="footer7-4">
            <div class="container">
                <div class="media-container-row align-center mbr-white">
                    <div class="row row-copirayt">
                        <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                            © Copyright 2020 Bomber Friends. Site created by Moussa !
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <!-- The Modal POUR LES PACKS -->
        <div class="modal" id="lespacks">
            <div class="modal-dialog  modal-xl">
                <div class="modal-content card-box packsmodal">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <img id="img_cof" src=""> <h3 class="modal-title" style="padding-left: 40px; "><span id="nom_cof"></span></h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <?php for ($i = 0; $i < 4; $i++) { ?>
                                    <div class="card-body col-4 col-md-3">
                                        <img id="fond_img<?php echo $i; ?>" class="img-fluid"  src="images/lescartes/fond_img.png"><br>
                                        <span id="reponse<?php echo $i; ?>"></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">FERMER</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- Les scritps -->
        <script src="assets/web/assets/jquery/jquery.min.js"></script>
        <script src="assets/popper/popper.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/smoothscroll/smooth-scroll.js"></script>
        <script src="assets/dropdown/js/nav-dropdown.js"></script>
        <script src="assets/dropdown/js/navbar-dropdown.js"></script>
        <script src="assets/tether/tether.min.js"></script>
        <script src="assets/playervimeo/vimeo_player.js"></script>
        <script src="assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
        <script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
        <script src="assets/parallax/jarallax.min.js"></script>
        <script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
        <script src="assets/theme/js/script.js"></script>
        <script type="text/javascript" src="js/packs.js"></script>
    </body>
</html>
